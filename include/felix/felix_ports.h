#pragma once

// Port calculation, DMA may be 0
#define PORT(OFFSET,DEVICE,DMA) ((OFFSET) + (10*DEVICE) + (DMA))

// Each tohost DMA buffer (up to 8 per device): 53100  + (10 * device) + dma
#define PORT_TOHOST_OFFSET (53100)

// Each toflx DMA buffer (up to 1 per device): 53200  + (10 * device) + dma
#define PORT_TOFLX_OFFSET (53200)

// TTC2H (1 per device): 53300 + (10*device) + dma
#define PORT_TTC2HOST_OFFSET (53300)

// DCS (1 per device): 53500 + (10*device) + dma
#define PORT_DCS_OFFSET (53500)

// Statistics, (1 per device): 53400 + (10 * device)
#define PORT_STATS_OFFSET 53400

// Errors, (1 per device): 53401 + (10 * device)
#define PORT_ERROR_OFFSET 53401

// Command/Reply (1 per device): 53402 + (10 * first-device)
#define PORT_COMMAND_OFFSET 53402

// Command/Reply (1 per device): 53403 + (10 * first-device)
#define PORT_REPLY_OFFSET 53403

// Monitoring port (1 per device): 53404 + (10 * first-device)
#define PORT_MON_OFFSET 53404

// No Longer Used
// #define PORT_TOFLX_BUSY (53180)

// #define PORT_REGISTER_CMD_D0 (53200)
// #define PORT_REGISTER_PUB_D0 (53220)

// #define PORT_REGISTER_CMD_D1 (53300)
// #define PORT_REGISTER_PUB_D1 (53320)

// #define ELINK_REGISTER (4040)
