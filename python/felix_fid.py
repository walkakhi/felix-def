# struct fid {
#   uint32_t       vid :  4;   // Version Id
#   uint32_t       did :  8;   // Detector Id
#   uint32_t       cid : 16;   // Connector Id
#   uint16_t       lid : 14;  // Link Id
#   uint8_t        tid : 14;   // Transport Id, 6 bits used for elink, bit 7 set if is_to_flx
#   uint8_t        sid : 8;   // Stream Id

import ctypes
c_uint = ctypes.c_uint
c_ushort = ctypes.c_ushort
c_uint16 = ctypes.c_uint16
c_uint32 = ctypes.c_uint32
c_uint64 = ctypes.c_uint64


class FIDbits(ctypes.LittleEndianStructure):
    _fields_ = [
            ("sid", c_uint64, 8),
            ("tid", c_uint64, 14),
            ("lid", c_uint64, 14),
            ("cid", c_uint64, 16),
            ("did", c_uint64, 8),
            ("vid", c_uint64, 4),
        ]


class FIDother(ctypes.LittleEndianStructure):
    _fields_ = [
            ("gid", c_uint64, 36),
            ("co", c_uint64,  28),
        ]


class FIDelink(ctypes.LittleEndianStructure):
    _fields_ = [
            ("sid", c_uint64, 8),
            ("protocol", c_uint64, 7),
            ("direction", c_uint64, 1),
            ("elink", c_uint64, 19),
            ("vlink", c_uint64, 1),
            ("cid", c_uint64, 16),
            ("did", c_uint64, 8),
            ("vid", c_uint64, 4),
        ]


class FIDstruct(ctypes.Union):
    _fields_ = [("b", FIDbits),
                ("o", FIDother),
                ("e", FIDelink),
                ("asLong", c_uint64)]


class FID:

    STATS_LINK = 0x1000
    ERROR_LINK = 0x1100
    COMMAND_REPLY_LINK = 0x1200
    MON_LINK = 0x1300

    @classmethod
    def get_fid(cls, detector_id, connector_id, elink, stream_id, is_to_flx=False, is_virtual=False):
        fid = FIDstruct()

        fid.b.vid = 1
        fid.b.did = detector_id
        fid.b.cid = connector_id

        fid.e.elink = elink
        fid.e.vlink = is_virtual
        fid.e.direction = is_to_flx

        fid.b.sid = stream_id

        return fid.asLong

    @classmethod
    def get_ctrl_fid(cls, fid):
        f = FIDstruct()
        f.asLong = fid
        return FID.get_fid(f.b.did, f.b.cid, 0x1200, 0, True, True)

    @classmethod
    def get_mon_fid(cls, fid):
        f = FIDstruct()
        f.asLong = fid
        return FID.get_fid(f.b.did, f.b.cid, 0x1300, 0, False, True)

    @classmethod
    def elink_from_lid_and_tid(cls, lid, tid):
        # only upper 6 bits out of TransportID are used for the elink
        return ((lid << 6) & 0x7FFC0) + ((tid >> 8) & 0x3F)

    @classmethod
    def elink_from_fid(cls, fid):
        # only upper 6 bits out of TransportID are used for the elink
        f = FIDstruct()
        f.asLong = fid
        return FID.elink_from_lid_and_tid(f.b.lid, f.b.tid)
